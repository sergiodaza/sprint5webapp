package com.dh.spring5webapp.repositories;

import com.dh.spring5webapp.model.Warehouse;
import org.springframework.data.repository.CrudRepository;

public interface WarehouseRepository extends CrudRepository<Warehouse, Long> {
}
    