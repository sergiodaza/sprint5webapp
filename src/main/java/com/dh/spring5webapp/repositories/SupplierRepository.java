package com.dh.spring5webapp.repositories;

import com.dh.spring5webapp.model.Supplier;
import org.springframework.data.repository.CrudRepository;

public interface SupplierRepository extends CrudRepository<Supplier, Long> {
}
    