package com.dh.spring5webapp.repositories;

import com.dh.spring5webapp.model.Incident;
import org.springframework.data.repository.CrudRepository;

public interface IncidentRepository extends CrudRepository<Incident, Long> {
}
    