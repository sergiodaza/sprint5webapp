package com.dh.spring5webapp.repositories;

import com.dh.spring5webapp.model.CategoryInc;
import org.springframework.data.repository.CrudRepository;

public interface CategoryIncRepository extends CrudRepository<CategoryInc, Long> {
}
    