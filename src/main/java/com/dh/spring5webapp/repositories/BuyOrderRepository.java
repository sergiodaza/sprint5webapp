package com.dh.spring5webapp.repositories;

import com.dh.spring5webapp.model.BuyOrder;
import org.springframework.data.repository.CrudRepository;

public interface BuyOrderRepository extends CrudRepository<BuyOrder, Long> {
}
    