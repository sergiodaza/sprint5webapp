package com.dh.spring5webapp.repositories;

import com.dh.spring5webapp.model.Comunication;
import org.springframework.data.repository.CrudRepository;

public interface ComunicationRepository extends CrudRepository<Comunication, Long> {
}
