package com.dh.spring5webapp.repositories;

import com.dh.spring5webapp.model.SubCategoryInc;
import org.springframework.data.repository.CrudRepository;

public interface SubCategoryIncRepository extends CrudRepository<SubCategoryInc, Long> {
}
    