package com.dh.spring5webapp.repositories;

import com.dh.spring5webapp.model.Inventory;
import org.springframework.data.repository.CrudRepository;

public interface InventoryRepository extends CrudRepository<Inventory, Long> {
}
    