package com.dh.spring5webapp.repositories;

import com.dh.spring5webapp.model.Comment;
import org.springframework.data.repository.CrudRepository;

public interface CommentRepository extends CrudRepository<Comment, Long> {
}
    