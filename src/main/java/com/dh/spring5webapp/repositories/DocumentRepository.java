package com.dh.spring5webapp.repositories;

import com.dh.spring5webapp.model.Document;
import org.springframework.data.repository.CrudRepository;

public interface DocumentRepository extends CrudRepository<Document, Long> {
}
