package com.dh.spring5webapp.repositories;

import com.dh.spring5webapp.model.Role;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends CrudRepository<Role, Long> {
}
