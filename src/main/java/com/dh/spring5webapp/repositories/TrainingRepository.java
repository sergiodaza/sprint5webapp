package com.dh.spring5webapp.repositories;

import com.dh.spring5webapp.model.Training;
import org.springframework.data.repository.CrudRepository;

public interface TrainingRepository extends CrudRepository<Training, Long> {
}
    