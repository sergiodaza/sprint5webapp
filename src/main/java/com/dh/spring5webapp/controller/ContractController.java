package com.dh.spring5webapp.controller;

import com.dh.spring5webapp.services.ContractService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ContractController {
    private ContractService contractService;

    public ContractController(ContractService contractService) {
        this.contractService = contractService;
    }

    @RequestMapping("/contracts")
    public String getContracts(Model model) {
        model.addAttribute("contracts", contractService.getContracts());
        return "contracts";
    }

}
