package com.dh.spring5webapp.controller;

import com.dh.spring5webapp.services.SupplierService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SupplierController {
    private SupplierService dataSupplierService;

    public SupplierController(SupplierService dataSupplierService) {
        this.dataSupplierService = dataSupplierService;
    }

    @RequestMapping("/suppliers")
    public String getSuppliers(Model model) {
        model.addAttribute("suppliers", dataSupplierService.getSuppliers());
        return "suppliers";
    }

}
