package com.dh.spring5webapp.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dh.spring5webapp.model.Category;
import com.dh.spring5webapp.model.SubCategory;
import com.dh.spring5webapp.repositories.CategoryRepository;
import com.dh.spring5webapp.repositories.SubCategoryRepository;

@Controller
public class IndexController {

    private CategoryRepository categoryRepository;
    private SubCategoryRepository subCategoryRepository;

    @RequestMapping({ "", "/", "/index" })
    public String getIndexPage() {
        Optional<Category> epp = categoryRepository.findByCode("EPP");
        System.out.println(epp.get().getName());

        Optional<SubCategory> safe = subCategoryRepository.findByCode("RM");
        System.out.println(safe.get().getName());

        return "index";
    }

    @Autowired  // NO ES BUENO
    public void setCategoryRepository(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Autowired
    public void setSubCategoryRepository(SubCategoryRepository subCategoryRepository) {
        this.subCategoryRepository = subCategoryRepository;
    }

    public CategoryRepository getCategoryRepository() {
        return categoryRepository;
    }

    public SubCategoryRepository getSubCategoryRepository() {
        return subCategoryRepository;
    }
}
