package com.dh.spring5webapp.controller;

import com.dh.spring5webapp.services.RegisterIncService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class RegisterIncController {
    private RegisterIncService dataRegisterIncService;

    public RegisterIncController(RegisterIncService dataRegisterIncService) {
        this.dataRegisterIncService = dataRegisterIncService;
    }

    @RequestMapping("/registerIncs")
    public String getRegisterIncs(Model model) {
        model.addAttribute("registerIncs", dataRegisterIncService.getRegisterIncs());
        return "registerIncs";
    }

}
