package com.dh.spring5webapp.controller;

import com.dh.spring5webapp.services.ComunicationService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ComunicationController {
    private ComunicationService dataComunicationService;

    public ComunicationController(ComunicationService dataComunicationService) {
        this.dataComunicationService = dataComunicationService;
    }

    @RequestMapping("/comunications")
    public String getComunications(Model model) {
        model.addAttribute("comunications", dataComunicationService.getComunications());
        return "comunications";
    }

}
