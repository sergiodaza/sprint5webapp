package com.dh.spring5webapp.controller;

import com.dh.spring5webapp.services.RoleService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class RoleController {
    private RoleService dataRoleService;

    public RoleController(RoleService dataRoleService) {
        this.dataRoleService = dataRoleService;
    }

    @RequestMapping("/roles")
    public String getRoles(Model model) {
        model.addAttribute("roles", dataRoleService.getRoles());
        return "roles";
    }

}
