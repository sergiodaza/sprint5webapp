package com.dh.spring5webapp.controller;

import com.dh.spring5webapp.services.BuyOrderService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class BuyOrderController {
    private BuyOrderService dataBuyOrderService;

    public BuyOrderController(BuyOrderService dataBuyOrderService) {
        this.dataBuyOrderService = dataBuyOrderService;
    }

    @RequestMapping("/buyOrders")
    public String getBuyOrders(Model model) {
        model.addAttribute("buyOrders", dataBuyOrderService.getBuyOrders());
        return "buyOrders";
    }

}
