package com.dh.spring5webapp.controller;

import com.dh.spring5webapp.services.WarehouseService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WarehouseController {
    private WarehouseService dataWarehouseService;

    public WarehouseController(WarehouseService dataWarehouseService) {
        this.dataWarehouseService = dataWarehouseService;
    }

    @RequestMapping("/warehouses")
    public String getWarehouses(Model model) {
        model.addAttribute("warehouses", dataWarehouseService.getWarehouses());
        return "warehouses";
    }

}
