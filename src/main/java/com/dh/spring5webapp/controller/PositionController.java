/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.dh.spring5webapp.controller;

import com.dh.spring5webapp.repositories.PositionRepository;
import com.dh.spring5webapp.services.PositionService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class PositionController {
    private PositionService positionService;

    public PositionController(PositionService positionService) {
        this.positionService = positionService;
    }

    @RequestMapping("/positions")
    public String getPositions(Model model) {
        model.addAttribute("positions", positionService.getPositions());
        return "positions";
    }

}
