package com.dh.spring5webapp.controller;

import com.dh.spring5webapp.services.TrainingService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TrainingController {
    private TrainingService dataTrainingService;

    public TrainingController(TrainingService dataTrainingService) {
        this.dataTrainingService = dataTrainingService;
    }

    @RequestMapping("/trainings")
    public String getTrainings(Model model) {
        model.addAttribute("trainings", dataTrainingService.getTrainings());
        return "trainings";
    }

}
