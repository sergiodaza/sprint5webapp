package com.dh.spring5webapp.controller;

import com.dh.spring5webapp.services.AreaService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AreaController {
    private AreaService dataAreaService;

    public AreaController(AreaService dataAreaService) {
        this.dataAreaService = dataAreaService;
    }

    @RequestMapping("/areas")
    public String getAreas(Model model) {
        model.addAttribute("areas", dataAreaService.getAreas());
        return "areas";
    }

}
