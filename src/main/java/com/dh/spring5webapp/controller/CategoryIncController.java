package com.dh.spring5webapp.controller;

import com.dh.spring5webapp.services.CategoryIncService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CategoryIncController {
    private CategoryIncService dataCategoryIncService;

    public CategoryIncController(CategoryIncService dataCategoryIncService) {
        this.dataCategoryIncService = dataCategoryIncService;
    }

    @RequestMapping("/categoryIncs")
    public String getCategoryIncs(Model model) {
        model.addAttribute("categoryIncs", dataCategoryIncService.getCategoryIncs());
        return "categoryIncs";
    }

}
