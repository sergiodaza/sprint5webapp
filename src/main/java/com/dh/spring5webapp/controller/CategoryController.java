package com.dh.spring5webapp.controller;

import com.dh.spring5webapp.services.CategoryService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CategoryController {
    private CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @RequestMapping("/categories")
    public String getCategories(Model model) {
        model.addAttribute("categories", categoryService.getCategories());
        return "categories";
    }

}
