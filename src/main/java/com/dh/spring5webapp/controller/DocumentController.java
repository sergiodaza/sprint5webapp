package com.dh.spring5webapp.controller;

import com.dh.spring5webapp.services.DocumentService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DocumentController {
    private DocumentService dataDocumentService;

    public DocumentController(DocumentService dataDocumentService) {
        this.dataDocumentService = dataDocumentService;
    }

    @RequestMapping("/documents")
    public String getDocuments(Model model) {
        model.addAttribute("documents", dataDocumentService.getDocuments());
        return "documents";
    }

}
