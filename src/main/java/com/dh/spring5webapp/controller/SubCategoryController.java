package com.dh.spring5webapp.controller;

import com.dh.spring5webapp.helpers.JsonTool;
import com.dh.spring5webapp.model.SubCategory;
import com.dh.spring5webapp.services.SubCategoryService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@Controller
public class SubCategoryController {
    private SubCategoryService dataService;
    private SubCategory item = null;
    private Class<SubCategory> resourceClass = SubCategory.class;
    private final String RESOURCE = "/subcategories";

    public SubCategoryController(SubCategoryService subCategoryService) {
        this.dataService = subCategoryService;
    }

    @RequestMapping(value = RESOURCE , method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String getAll() {
        return JsonTool.allToJson(dataService.getAll());
    }

    @RequestMapping(value = RESOURCE+"/{id}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String getOne(@PathVariable("id") long id) {
        return JsonTool.oneToJson(dataService.getOne(id));
    }

    @RequestMapping(value = RESOURCE, method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    String create(@RequestBody String body) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        this.item = mapper.readValue(body, resourceClass);
        return JsonTool.oneToJson(dataService.save(this.item));
    }

    @RequestMapping(value = RESOURCE+"/{id}", method = RequestMethod.PUT, produces = "application/json")
    public @ResponseBody
    String update(@PathVariable("id") long id, @RequestBody String body) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        this.item = mapper.readValue(body, resourceClass);
        this.item.setId(id);
        return JsonTool.oneToJson(dataService.save(item));
    }

    @RequestMapping(value = RESOURCE+"/{id}", method = RequestMethod.DELETE, produces = "application/json")
    public @ResponseBody
    boolean delete(@PathVariable("id") long id) {
        return dataService.delete(id);
    }

}
