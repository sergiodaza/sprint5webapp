package com.dh.spring5webapp.controller;

import com.dh.spring5webapp.services.IncidentService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IncidentController {
    private IncidentService dataIncidentService;

    public IncidentController(IncidentService dataIncidentService) {
        this.dataIncidentService = dataIncidentService;
    }

    @RequestMapping("/incidents")
    public String getIncidents(Model model) {
        model.addAttribute("incidents", dataIncidentService.getIncidents());
        return "incidents";
    }

}
