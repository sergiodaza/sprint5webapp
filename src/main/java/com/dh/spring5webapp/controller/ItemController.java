/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.dh.spring5webapp.controller;

import com.dh.spring5webapp.helpers.JsonTool;
import com.dh.spring5webapp.model.Item;
import com.dh.spring5webapp.services.ItemService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@CrossOrigin
@Controller
public class ItemController {

    private ItemService dataService;
    private Item item = null;
    private Class<Item> resourceClass = Item.class;
    private final String RESOURCE = "/items";

    public ItemController(ItemService itemService) {
        this.dataService = itemService;
    }

    @RequestMapping(value = RESOURCE , method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String getAll() {
        return JsonTool.allToJson(dataService.getAll());
    }

    @RequestMapping(value = RESOURCE+"/{id}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String getOne(@PathVariable("id") long id) {
        return JsonTool.oneToJson(dataService.getOne(id));
    }

    @RequestMapping(value = RESOURCE, method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    String create(@RequestBody String body) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        this.item = mapper.readValue(body, resourceClass);
        return JsonTool.oneToJson(dataService.save(this.item));
    }

    @RequestMapping(value = RESOURCE+"/{id}", method = RequestMethod.PUT, produces = "application/json")
    public @ResponseBody
    String update(@PathVariable("id") long id, @RequestBody String body) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        this.item = mapper.readValue(body, resourceClass);
        this.item.setId(id);
        return JsonTool.oneToJson(dataService.save(item));
    }

    @RequestMapping(value = RESOURCE+"/{id}", method = RequestMethod.DELETE, produces = "application/json")
    public @ResponseBody
    boolean delete(@PathVariable("id") long id) {
        return dataService.delete(id);
    }

}
