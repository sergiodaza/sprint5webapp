package com.dh.spring5webapp.controller;

import com.dh.spring5webapp.services.SubCategoryIncService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SubCategoryIncController {
    private SubCategoryIncService dataSubCategoryIncService;

    public SubCategoryIncController(SubCategoryIncService dataSubCategoryIncService) {
        this.dataSubCategoryIncService = dataSubCategoryIncService;
    }

    @RequestMapping("/subCategoryIncs")
    public String getSubCategoryIncs(Model model) {
        model.addAttribute("subCategoryIncs", dataSubCategoryIncService.getSubCategoryIncs());
        return "subCategoryIncs";
    }

}
