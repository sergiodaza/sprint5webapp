package com.dh.spring5webapp.controller;

import com.dh.spring5webapp.services.InventoryService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class InventoryController {
    private InventoryService dataInventoryService;

    public InventoryController(InventoryService dataInventoryService) {
        this.dataInventoryService = dataInventoryService;
    }

    @RequestMapping("/inventories")
    public String getInventorys(Model model) {
        model.addAttribute("inventories", dataInventoryService.getInventories());
        return "inventories";
    }

}
