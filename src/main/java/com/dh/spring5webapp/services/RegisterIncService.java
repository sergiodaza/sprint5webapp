package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.RegisterInc;

import java.util.Set;

public interface RegisterIncService {
    Set<RegisterInc> getRegisterIncs();
}
