package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.Employee;
import com.dh.spring5webapp.repositories.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository dataEmployeeRepository;

    @Override
    public Set<Employee> getEmployees() {

        Set<Employee> result = new HashSet<>();

        dataEmployeeRepository.findAll().iterator().forEachRemaining(result::add);
        return result;
    }
}
