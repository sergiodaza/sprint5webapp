package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.Category;
import java.util.Set;

public interface CategoryService {
    Set<Category> getCategories();
}
