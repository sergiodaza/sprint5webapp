package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.Incident;
import com.dh.spring5webapp.repositories.IncidentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class IncidentServiceImpl implements IncidentService {

    @Autowired
    private IncidentRepository dataIncidentRepository;

    @Override
    public Set<Incident> getIncidents() {

        Set<Incident> result = new HashSet<>();

        dataIncidentRepository.findAll().iterator().forEachRemaining(result::add);
        return result;
    }
}
