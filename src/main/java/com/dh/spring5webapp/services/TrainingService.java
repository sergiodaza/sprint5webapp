package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.Training;

import java.util.Set;

public interface TrainingService {
    Set<Training> getTrainings();
}
