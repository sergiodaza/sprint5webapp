package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.Contract;
import com.dh.spring5webapp.repositories.ContractRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class ContractServiceImpl implements ContractService {

    @Autowired
    private ContractRepository dataContractRepository;

    @Override
    public Set<Contract> getContracts() {

        Set<Contract> result = new HashSet<>();

        dataContractRepository.findAll().iterator().forEachRemaining(result::add);
        return result;
    }
}
