package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.BuyOrder;
import com.dh.spring5webapp.repositories.BuyOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class BuyOrderServiceImpl implements BuyOrderService {

    @Autowired
    private BuyOrderRepository dataBuyOrderRepository;

    @Override
    public Set<BuyOrder> getBuyOrders() {

        Set<BuyOrder> result = new HashSet<>();

        dataBuyOrderRepository.findAll().iterator().forEachRemaining(result::add);
        return result;
    }
}
