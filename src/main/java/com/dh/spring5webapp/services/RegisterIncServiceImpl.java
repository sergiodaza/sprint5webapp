package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.RegisterInc;
import com.dh.spring5webapp.repositories.RegisterIncRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class RegisterIncServiceImpl implements RegisterIncService {

    @Autowired
    private RegisterIncRepository dataRegisterIncRepository;

    @Override
    public Set<RegisterInc> getRegisterIncs() {

        Set<RegisterInc> result = new HashSet<>();

        dataRegisterIncRepository.findAll().iterator().forEachRemaining(result::add);
        return result;
    }
}
