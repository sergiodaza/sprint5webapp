package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.Comunication;

import java.util.Set;

public interface ComunicationService {
    Set<Comunication> getComunications();
}
