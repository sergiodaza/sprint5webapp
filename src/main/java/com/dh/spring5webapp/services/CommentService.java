package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.Comment;

import java.util.Set;

public interface CommentService {
    Set<Comment> getAll();
    Comment getOne(long id);
    Comment save(Comment item);
    boolean delete(long id);
}
