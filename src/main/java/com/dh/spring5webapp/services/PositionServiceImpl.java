package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.Position;
import com.dh.spring5webapp.repositories.PositionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class PositionServiceImpl implements PositionService {

    @Autowired
    private PositionRepository dataPositionRepository;

    @Override
    public Set<Position> getPositions() {

        Set<Position> result = new HashSet<>();

        dataPositionRepository.findAll().iterator().forEachRemaining(result::add);
        return result;
    }
}
