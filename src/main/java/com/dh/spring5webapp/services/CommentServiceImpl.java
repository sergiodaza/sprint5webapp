package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.Comment;
import com.dh.spring5webapp.repositories.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentRepository dataCommentRepository;

    @Override
    public Set<Comment> getAll() {
        Set<Comment> result = new HashSet<>();
        dataCommentRepository.findAll().iterator().forEachRemaining(result::add);
        return result;
    }

    @Override
    public Comment getOne(long id) {
        return dataCommentRepository.findOne(id);
    }

    @Override
    public Comment save(Comment comment) {
        return dataCommentRepository.save(comment);
    }

    @Override
    public boolean delete(long id) {
        if (dataCommentRepository.exists(id)) {
            dataCommentRepository.delete(id);
            return true;
        } else {
            return false;
        }
    }
}
