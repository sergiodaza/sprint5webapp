package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.Position;

import java.util.Set;

public interface PositionService {
    Set<Position> getPositions();
}
