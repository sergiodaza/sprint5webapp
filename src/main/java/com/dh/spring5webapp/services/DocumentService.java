package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.Document;

import java.util.Set;

public interface DocumentService {
    Set<Document> getDocuments();
}
