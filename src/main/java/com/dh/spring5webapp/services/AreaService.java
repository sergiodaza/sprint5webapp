package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.Area;

import java.util.Set;

public interface AreaService {
    Set<Area> getAreas();
}
