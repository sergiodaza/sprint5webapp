package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.Role;
import com.dh.spring5webapp.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepository dataRoleRepository;

    @Override
    public Set<Role> getRoles() {

        Set<Role> result = new HashSet<>();

        dataRoleRepository.findAll().iterator().forEachRemaining(result::add);
        return result;
    }
}
