package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.Employee;

import java.util.Set;

public interface EmployeeService {
    Set<Employee> getEmployees();
}
