package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.CategoryInc;
import com.dh.spring5webapp.repositories.CategoryIncRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class CategoryIncServiceImpl implements CategoryIncService {

    @Autowired
    private CategoryIncRepository dataCategoryIncRepository;

    @Override
    public Set<CategoryInc> getCategoryIncs() {

        Set<CategoryInc> result = new HashSet<>();

        dataCategoryIncRepository.findAll().iterator().forEachRemaining(result::add);
        return result;
    }
}
