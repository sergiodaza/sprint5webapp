package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.SubCategoryInc;
import com.dh.spring5webapp.repositories.SubCategoryIncRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class SubCategoryIncServiceImpl implements SubCategoryIncService {

    @Autowired
    private SubCategoryIncRepository dataSubCategoryIncRepository;

    @Override
    public Set<SubCategoryInc> getSubCategoryIncs() {

        Set<SubCategoryInc> result = new HashSet<>();

        dataSubCategoryIncRepository.findAll().iterator().forEachRemaining(result::add);
        return result;
    }
}
