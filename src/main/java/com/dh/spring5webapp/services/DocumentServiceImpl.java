package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.Document;
import com.dh.spring5webapp.repositories.DocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class DocumentServiceImpl implements DocumentService {

    @Autowired
    private DocumentRepository dataDocumentRepository;

    @Override
    public Set<Document> getDocuments() {

        Set<Document> result = new HashSet<>();

        dataDocumentRepository.findAll().iterator().forEachRemaining(result::add);
        return result;
    }
}
