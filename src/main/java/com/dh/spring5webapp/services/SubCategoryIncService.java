package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.SubCategoryInc;

import java.util.Set;

public interface SubCategoryIncService {
    Set<SubCategoryInc> getSubCategoryIncs();
}
