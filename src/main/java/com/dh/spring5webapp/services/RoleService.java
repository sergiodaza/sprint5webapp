package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.Role;

import java.util.Set;

public interface RoleService {
    Set<Role> getRoles();
}
