package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.Item;

import java.util.Set;

public interface ItemService {
    Set<Item> getAll();
    Item getOne(long id);
    Item save(Item item);
    boolean delete(long id);
}
