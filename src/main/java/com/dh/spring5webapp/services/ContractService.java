package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.Contract;

import java.util.Set;

public interface ContractService {
    Set<Contract> getContracts();
}
