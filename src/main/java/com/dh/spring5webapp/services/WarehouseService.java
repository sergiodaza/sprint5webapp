package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.Warehouse;

import java.util.Set;

public interface WarehouseService {
    Set<Warehouse> getWarehouses();
}
