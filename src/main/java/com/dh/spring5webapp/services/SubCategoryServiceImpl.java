package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.SubCategory;
import com.dh.spring5webapp.repositories.SubCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class SubCategoryServiceImpl implements SubCategoryService {

    @Autowired
    private SubCategoryRepository dataSubCategoryRepository;

    @Override
    public Set<SubCategory> getAll() {
        Set<SubCategory> result = new HashSet<>();
        dataSubCategoryRepository.findAll().iterator().forEachRemaining(result::add);
        return result;
    }

    @Override
    public SubCategory getOne(long id) {
        return dataSubCategoryRepository.findOne(id);
    }

    @Override
    public SubCategory save(SubCategory item) {
        return dataSubCategoryRepository.save(item);
    }

    @Override
    public boolean delete(long id) {
        if (dataSubCategoryRepository.exists(id)) {
            dataSubCategoryRepository.delete(id);
            return true;
        } else {
            return false;
        }
    }
}
