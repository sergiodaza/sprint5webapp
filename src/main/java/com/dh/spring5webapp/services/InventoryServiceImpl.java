package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.Inventory;
import com.dh.spring5webapp.repositories.InventoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class InventoryServiceImpl implements InventoryService {

    @Autowired
    private InventoryRepository dataInventoryRepository;

    @Override
    public Set<Inventory> getInventories() {

        Set<Inventory> result = new HashSet<>();

        dataInventoryRepository.findAll().iterator().forEachRemaining(result::add);
        return result;
    }
}
