package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.Training;
import com.dh.spring5webapp.repositories.TrainingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class TrainingServiceImpl implements TrainingService {

    @Autowired
    private TrainingRepository dataTrainingRepository;

    @Override
    public Set<Training> getTrainings() {

        Set<Training> result = new HashSet<>();

        dataTrainingRepository.findAll().iterator().forEachRemaining(result::add);
        return result;
    }
}
