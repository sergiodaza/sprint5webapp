package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.Supplier;
import com.dh.spring5webapp.repositories.SupplierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    private SupplierRepository dataSupplierRepository;

    @Override
    public Set<Supplier> getSuppliers() {

        Set<Supplier> result = new HashSet<>();

        dataSupplierRepository.findAll().iterator().forEachRemaining(result::add);
        return result;
    }
}
