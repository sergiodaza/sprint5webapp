package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.CategoryInc;

import java.util.Set;

public interface CategoryIncService {
    Set<CategoryInc> getCategoryIncs();
}
