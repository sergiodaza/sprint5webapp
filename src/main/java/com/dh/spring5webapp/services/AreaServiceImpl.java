package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.Area;
import com.dh.spring5webapp.repositories.AreaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class AreaServiceImpl implements AreaService {

    @Autowired
    private AreaRepository dataAreaRepository;

    @Override
    public Set<Area> getAreas() {

        Set<Area> result = new HashSet<>();

        dataAreaRepository.findAll().iterator().forEachRemaining(result::add);
        return result;
    }
}
