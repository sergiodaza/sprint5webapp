package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.Item;
import com.dh.spring5webapp.repositories.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

@Service
public class ItemServiceImpl implements ItemService {

    @Autowired
    private ItemRepository dataItemRepository;

    @Override
    public Set<Item> getAll() {
        Set<Item> result = new HashSet<>();
        dataItemRepository.findAll().iterator().forEachRemaining(result::add);
        return result;
    }

    @Override
    public Item getOne(long id) {
        return dataItemRepository.findOne(id);
    }

    @Override
    public Item save(Item item) {
        return dataItemRepository.save(item);
    }

    @Override
    public boolean delete(long id) {
        if (dataItemRepository.exists(id)) {
            dataItemRepository.delete(id);
            return true;
        } else {
            return false;
        }
    }
}
