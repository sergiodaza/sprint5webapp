package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.Supplier;

import java.util.Set;

public interface SupplierService {
    Set<Supplier> getSuppliers();
}
