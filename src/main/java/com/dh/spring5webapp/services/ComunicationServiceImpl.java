package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.Comunication;
import com.dh.spring5webapp.repositories.ComunicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class ComunicationServiceImpl implements ComunicationService {

    @Autowired
    private ComunicationRepository dataComunicationRepository;

    @Override
    public Set<Comunication> getComunications() {

        Set<Comunication> result = new HashSet<>();

        dataComunicationRepository.findAll().iterator().forEachRemaining(result::add);
        return result;
    }
}
