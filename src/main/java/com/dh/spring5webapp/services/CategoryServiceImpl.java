package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.Category;
import com.dh.spring5webapp.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public Set<Category> getCategories() {

        Set<Category> categories = new HashSet<>();

        categoryRepository.findAll().iterator().forEachRemaining(categories::add);
        return categories;
    }
}
