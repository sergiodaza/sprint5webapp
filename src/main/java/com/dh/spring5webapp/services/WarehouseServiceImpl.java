package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.Warehouse;
import com.dh.spring5webapp.repositories.WarehouseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class WarehouseServiceImpl implements WarehouseService {

    @Autowired
    private WarehouseRepository dataWarehouseRepository;

    @Override
    public Set<Warehouse> getWarehouses() {

        Set<Warehouse> result = new HashSet<>();

        dataWarehouseRepository.findAll().iterator().forEachRemaining(result::add);
        return result;
    }
}
