package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.SubCategory;

import java.util.Set;

public interface SubCategoryService {
    Set<SubCategory> getAll();
    SubCategory getOne(long id);
    SubCategory save(SubCategory item);
    boolean delete(long id);
}
