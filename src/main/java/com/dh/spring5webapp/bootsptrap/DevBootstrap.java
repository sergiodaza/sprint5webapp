package com.dh.spring5webapp.bootsptrap;

import com.dh.spring5webapp.model.*;
import com.dh.spring5webapp.repositories.*;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.Calendar;
import java.util.HashSet;

@Component
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent> {
    private CategoryRepository categoryRepository;
    private SubCategoryRepository subCategoryRepository;
    private ItemRepository itemRepository;
    private EmployeeRepository employeeRepository;
    private PositionRepository positionRepository;
    private ContractRepository contractRepository;
    private DocumentRepository documentRepository;
    private ComunicationRepository comunicationRepository;
    private RoleRepository roleRepository;
    private TrainingRepository trainingRepository;
    private AreaRepository areaRepository;
    private InventoryRepository inventoryRepository;
    private WarehouseRepository warehouseRepository;
    private BuyOrderRepository buyOrderRepository;
    private SupplierRepository supplierRepository;
    private IncidentRepository incidentRepository;
    private CategoryIncRepository categoryIncRepository;
    private SubCategoryIncRepository subCategoryIncRepository;
    private RegisterIncRepository registerIncRepository;
    private CommentRepository commentRepository;

    public DevBootstrap(CategoryRepository categoryRepository, SubCategoryRepository subCategoryRepository,
                        ItemRepository itemRepository, EmployeeRepository employeeRepository, PositionRepository positionRepository,
                        ContractRepository contractRepository, DocumentRepository documentRepository, ComunicationRepository comunicationRepository, RoleRepository roleRepository, TrainingRepository trainingRepository, AreaRepository areaRepository, InventoryRepository inventoryRepository, WarehouseRepository warehouseRepository, BuyOrderRepository buyOrderRepository, SupplierRepository supplierRepository, IncidentRepository incidentRepository, CategoryIncRepository categoryIncRepository, SubCategoryIncRepository subCategoryIncRepository, RegisterIncRepository registerIncRepository, CommentRepository commentRepository) {
        this.categoryRepository = categoryRepository;
        this.subCategoryRepository = subCategoryRepository;
        this.itemRepository = itemRepository;
        this.employeeRepository = employeeRepository;
        this.positionRepository = positionRepository;
        this.contractRepository = contractRepository;
        this.documentRepository = documentRepository;
        this.comunicationRepository = comunicationRepository;
        this.roleRepository = roleRepository;
        this.trainingRepository = trainingRepository;
        this.areaRepository = areaRepository;
        this.inventoryRepository = inventoryRepository;
        this.warehouseRepository = warehouseRepository;
        this.buyOrderRepository = buyOrderRepository;
        this.supplierRepository = supplierRepository;
        this.incidentRepository = incidentRepository;
        this.categoryIncRepository = categoryIncRepository;
        this.subCategoryIncRepository = subCategoryIncRepository;
        this.registerIncRepository = registerIncRepository;
        this.commentRepository = commentRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        initData();
    }

    private void initData() {

        Category eppCategory = new Category();
        eppCategory.setName("Personal Protection Equipment");
        eppCategory.setCode("EPP");

        // EPP category
        Category resourceCategory = new Category();
        resourceCategory.setCode("RES");
        resourceCategory.setName("RESOURCE");

        // safety subcategory
        SubCategory safetySubCategory = new SubCategory();
        safetySubCategory.setCategory(eppCategory);
        safetySubCategory.setCode("SAF");
        safetySubCategory.setName("SAFETY");

        HashSet<SubCategory> subCategoryHashSet = new HashSet<>();
        subCategoryHashSet.add(safetySubCategory);

        eppCategory.setSubCategory(subCategoryHashSet);
        categoryRepository.save(eppCategory);
        categoryRepository.save(resourceCategory);
        subCategoryRepository.save(safetySubCategory);

        // raw material subcategory
        SubCategory rawMaterialSubCategory = new SubCategory();
        rawMaterialSubCategory.setCategory(resourceCategory);
        rawMaterialSubCategory.setCode("RM");
        rawMaterialSubCategory.setName("RAW MATERIAL");

        subCategoryRepository.save(rawMaterialSubCategory);

        // Comments
        Comment commentHelmet = new Comment();
        commentHelmet.setComment("this is a comment to helmet");
        commentHelmet.setAuthor("Sergio Daza");
        Calendar commentHelmetDate = Calendar.getInstance();
        commentHelmetDate.set(2010, Calendar.JANUARY, 1);
        commentHelmet.setDate(commentHelmetDate.getTime());
        commentHelmet.setRating(5);
        commentRepository.save(commentHelmet);

        // Helmet Item
        Item helmet = new Item();
        helmet.setCode("HEL");
        helmet.setName("HELMET");
        helmet.setFeatured(false);
        helmet.setLabel("label helmet");
        helmet.setDescription("helmet description");
        // get image
        File f = new File("./src/main/resources/images/placeholder.png"); //asociamos el archivo fisico
        InputStream is = null; //lo abrimos. Lo importante es que sea un InputStream
        try {
            is = new FileInputStream(f);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        byte[] buffer = new byte[(int) f.length()]; //creamos el buffer
        try {
            int readers = is.read(buffer); //leemos el archivo al buffer
        } catch (IOException e) {
            e.printStackTrace();
        }

        helmet.setImage(buffer);
        helmet.getComments().add(commentHelmet);
        helmet.setSubCategory(safetySubCategory);


        itemRepository.save(helmet);

        // Warehouse
        Warehouse warehouse = new Warehouse();
        warehouse.setName("blue");
        warehouse.setCode("whb");
        warehouseRepository.save(warehouse);

        // ink Item
        Item ink = new Item();
        ink.setCode("INK");
        ink.setName("INK");
        ink.setFeatured(false);
        ink.setLabel("label ink");
        ink.setDescription("ink description");
        ink.setImage(buffer);
        ink.setSubCategory(rawMaterialSubCategory);
        itemRepository.save(ink);

        // Inventory
        Inventory inventoryInk = new Inventory();
        inventoryInk.setQuantity(10);
        inventoryInk.setStatus("reception");
        inventoryInk.setItem(ink);
        inventoryInk.setWarehouse(warehouse);
        inventoryRepository.save(inventoryInk);


        // John Employee
        Employee jhon = new Employee();
        jhon.setFirstName("John");
        jhon.setLastName("Doe");
        Document johnDocument = new Document();
        johnDocument.setType("ci");
        johnDocument.setCode("1234567890");
        documentRepository.save(johnDocument);
        jhon.setDocument(johnDocument);
        Comunication johnComunication = new Comunication();
        johnComunication.setType("movil");
        johnComunication.setValue("77777777");
        comunicationRepository.save(johnComunication);
        jhon.getComunication().add(johnComunication);


        // Position
        Role role = new Role();
        role.setName("role1");
        roleRepository.save(role);

        Position position = new Position();
        position.setName("OPERATIVE");
        position.getRoles().add(role);
        positionRepository.save(position);

        //Area
        Area areaLobby = new Area();
        areaLobby.setName("lobby");
        areaLobby.setCode("lby");
        areaRepository.save(areaLobby);

        Area area001 = new Area();
        area001.setName("area001");
        area001.setCode("a001");
        areaRepository.save(area001);

        //Training
        Training trainingOne = new Training();
        trainingOne.setSkill("Unit Test");
        trainingOne.setPosition(position);
        Calendar trainingDate = Calendar.getInstance();
        trainingDate.set(2010, Calendar.JANUARY, 1);
        trainingOne.setDate(trainingDate.getTime());
        trainingOne.setArea(areaLobby);
        trainingRepository.save(trainingOne);

        // contract
        Contract contract = new Contract();
        contract.setEmployee(jhon);
        Calendar contracInitDate = Calendar.getInstance();
        contracInitDate.set(2010, Calendar.JANUARY, 1);
        contract.setInitDate(contracInitDate.getTime());
        contract.setPosition(position);

        jhon.getContracts().add(contract);
        employeeRepository.save(jhon);
        contractRepository.save(contract);

        // Supplier
        Supplier supplier001 = new Supplier();
        supplier001.setName("supplier001");
        supplier001.setCode("s001");
        supplierRepository.save(supplier001);

        // BuyOrder
        BuyOrder buyOrderInk = new BuyOrder();
        buyOrderInk.setUnitType("lt");
        buyOrderInk.setStatus("in progress");
        buyOrderInk.getItems().add(ink);
        buyOrderInk.setSupplier(supplier001);
        buyOrderRepository.save(buyOrderInk);

        // CategoryInc
        CategoryInc categoryInc001 = new CategoryInc();
        categoryInc001.setName("CategoryInc001");
        categoryInc001.setCode("CI001");
        categoryIncRepository.save(categoryInc001);

        // SubCategoryInc
        SubCategoryInc subCategoryInc001 = new SubCategoryInc();
        subCategoryInc001.setName("SubCategoryInc001");
        subCategoryInc001.setCode("SCI001");
        subCategoryInc001.setCategoryInc(categoryInc001);
        subCategoryIncRepository.save(subCategoryInc001);

        // Incident
        Incident incident001 = new Incident();
        incident001.setName("incident0001");
        incident001.setCode("Inc001");
        incident001.setSubCategoryInc(subCategoryInc001);
        incidentRepository.save(incident001);

        // RegisterInc
        RegisterInc registerInc001 = new RegisterInc();
        Calendar registerIncDate = Calendar.getInstance();
        registerIncDate.set(2010, Calendar.JANUARY, 1);
        registerInc001.setDate(registerIncDate.getTime());
        registerInc001.setDescription("Description of register 001");
        registerInc001.setQuantification("quantification 001");
        registerInc001.setIncident(incident001);
        registerInc001.getEmployees().add(jhon);
        registerInc001.getAreas().add(areaLobby);
        registerInc001.getAreas().add(area001);
        registerIncRepository.save(registerInc001);
    }
}
