package com.dh.spring5webapp.model;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class SubCategoryInc extends ModelBase {
    private String name;
    private String code;

    @OneToOne(optional = false)
    private CategoryInc categoryInc;

    public CategoryInc getCategoryInc() {
        return categoryInc;
    }

    public void setCategoryInc(CategoryInc categoryInc) {
        this.categoryInc = categoryInc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}