package com.dh.spring5webapp.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Item extends ModelBase {
    private String name;
    private String code;
    private boolean featured;
    private String label;
    private double price;
    private String description;
    //private String image;
    @Lob
    @Column(name = "image")
    private byte[] image;

    @OneToMany(fetch = FetchType.LAZY)
    private Set<Comment> comments = new HashSet<>();

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public boolean isFeatured() {
        return featured;
    }

    public void setFeatured(boolean featured) {
        this.featured = featured;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @OneToOne
    private SubCategory subCategory;

    @ManyToMany(fetch = FetchType.LAZY)
    private Set<BuyOrder> buyOrders = new HashSet<>();

    public Set<BuyOrder> getBuyOrders() {
        return buyOrders;
    }

    public void setBuyOrders(Set<BuyOrder> buyOrders) {
        this.buyOrders = buyOrders;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public SubCategory getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(SubCategory subCategory) {
        this.subCategory = subCategory;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }
}
