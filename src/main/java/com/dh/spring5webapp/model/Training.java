package com.dh.spring5webapp.model;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Training extends ModelBase {
    private String Skill;

    @Temporal(TemporalType.DATE)
    private Date date;

    @OneToOne(optional = false)
    private  Position position;

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    @OneToOne(optional = false)
    private Area area;


    public String getSkill() {
        return Skill;
    }

    public void setSkill(String skill) {
        Skill = skill;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }
}