package com.dh.spring5webapp.model;

import javax.persistence.Entity;

@Entity
public class Document extends ModelBase {
    private String Type;
    private String Code;

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }
}
