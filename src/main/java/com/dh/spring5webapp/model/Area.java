package com.dh.spring5webapp.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Area extends ModelBase {
    private String name;
    private String code;

    @ManyToMany(fetch = FetchType.LAZY)
    private Set<RegisterInc> registerIncs = new HashSet<>();

    public Set<RegisterInc> getRegisterIncs() {
        return registerIncs;
    }

    public void setRegisterIncs(Set<RegisterInc> registerIncs) {
        this.registerIncs = registerIncs;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}