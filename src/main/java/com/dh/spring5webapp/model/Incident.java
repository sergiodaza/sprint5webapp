package com.dh.spring5webapp.model;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class Incident extends ModelBase {
    private String name;
    private String code;

    @OneToOne(optional = false)
    private SubCategoryInc subCategoryInc;

    public SubCategoryInc getSubCategoryInc() {
        return subCategoryInc;
    }

    public void setSubCategoryInc(SubCategoryInc subCategoryInc) {
        this.subCategoryInc = subCategoryInc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}