/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.dh.spring5webapp.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Employee extends ModelBase {
    private String firstName;
    private String lastName;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "employee")
    private Set<Contract> contracts = new HashSet<>();

    @OneToOne(cascade = {})
    private Document document;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "employee")
    private Set<Comunication> comunication = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY)
    private Set<RegisterInc> registerIncs = new HashSet<>();

    public Set<RegisterInc> getRegisterIncs() {
        return registerIncs;
    }

    public void setRegisterIncs(Set<RegisterInc> registerIncs) {
        this.registerIncs = registerIncs;
    }

    public Set<Comunication> getComunication() {
        return comunication;
    }

    public void setComunication(Set<Comunication> comunication) {
        this.comunication = comunication;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Set<Contract> getContracts() {
        return contracts;
    }

    public void setContracts(Set<Contract> contracts) {
        this.contracts = contracts;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }
}
