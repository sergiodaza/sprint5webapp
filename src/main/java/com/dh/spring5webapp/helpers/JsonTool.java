package com.dh.spring5webapp.helpers;


import com.dh.spring5webapp.model.Item;
import com.dh.spring5webapp.services.ItemService;
import com.fasterxml.jackson.databind.ObjectMapper;


import javax.persistence.SecondaryTable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class JsonTool {

    public static String oneToJson(Object object) {
        String jsonStr = "null";
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            jsonStr = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return jsonStr;
    }

    public static String allToJson(Set<?> items) {
        List<String> response = new ArrayList<String>();
        items.iterator().forEachRemaining(object -> response.add(JsonTool.oneToJson(object)));
        return response.toString();
    }

    public static Object jStringToObject(String body) {
        Object result = null;
        ObjectMapper mapper = new ObjectMapper();
        try {
            result = mapper.readValue(body, Object.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}
