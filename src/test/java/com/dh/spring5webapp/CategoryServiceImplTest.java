package com.dh.spring5webapp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.HashSet;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.dh.spring5webapp.model.Category;
import com.dh.spring5webapp.repositories.CategoryRepository;
import com.dh.spring5webapp.services.CategoryServiceImpl;

/**
 * CategoryServiceImpl Tester.
 *
 * @author <Authors name>
 * @version 1.0
 * @since <pre>Dec 11, 2017</pre>
 */
@RunWith(MockitoJUnitRunner.class)
public class CategoryServiceImplTest {
    private static final String OTRACAT = "OTRACAT";
    @InjectMocks  // a este objeto se injectaran todos los mocks
            CategoryServiceImpl categoryService;

    @Mock
    CategoryRepository categoryRepository;

    @Before
    public void before() throws Exception {
        Set<Category> categorySet = new HashSet<>();
        Category category = new Category();
        category.setName(OTRACAT);
        categorySet.add(category);

        when(categoryRepository.findAll()).thenReturn(categorySet);
    }

    @After
    public void after() throws Exception {
    }

    /**
     * Method: getCategories()
     */
    @Test
    public void testGetCategories() throws Exception {
        Set<Category> categories = categoryService.getCategories();
        assertTrue(categories.size() == 1);
        assertEquals(OTRACAT, categories.iterator().next().getName());
    }

}
